CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module is intended to create a system for displaying and managing "quick
links" on your website, as are often found on the home page.

As these are usually simple and not intended to be accessed directly on their
own, they have been configured as Storage Entities, so they won't clutter up the
Add Content menu, need to be excluded from any site search configuration, etc.

In addition to creating a bundle for managing quick links, it also provides a
view block for presenting them in a grid, and a page view to allow drag-and-drop
sorting using DraggableViews.

Icons can be chosen from the media library for each link, using SVG images. Note
that the installed configurations are just a starting point, and you can alter
or extend the configuration to meet your site's needs.


INSTALLATION
------------

 * Install the Quick Links module as you would normally install a
   contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
   further information. We strongly recommend using composer to ensure all
   dependencies will be handled automatically.
 * This will import configuration. Once installed, the module doesn't really
   provide any additional functionality, so it can be uninstalled. Note that
   you won't be able to install it again on the same site, unless you delete the
   Quick Link bundle and Quick Links view that were installed originally.
 * There is also a Quick Links Olivero submodule that is recommended if using
   Quick Links on a site using the Olivero theme. If using a custom theme, you
   may need to implement similar CSS to what is found in the submodule. When
   installed, the submodule also automatically puts the Quick Links block above
   the content only on the site home page, so if you're not using it you will
   need to place this block and configure it yourself.


REQUIREMENTS
------------

This module requires the Storage Entities, Add Content By Bundle, Display Link
Plus, and DraggableViews modules.


CONFIGURATION
-------------

 * If you'd like to tweak the size of the icons and the grid, these are settings
   in the Quick Links view. The size of the icons can be changed through the
   "Image width" and "Image height" attributes in the "(field_icon: Media)
   Media: SVG" field. The grid layout can be tweaked in the Responsive Grid
   settings, including the maximum number of columns, the minimum cell width,
   and more.


MAINTAINERS
-----------

 * Current Maintainer: Martin Anderson-Clutz (mandclu) - https://www.drupal.org/u/mandclu
